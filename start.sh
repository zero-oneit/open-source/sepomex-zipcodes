#!/usr/bin/env sh
DEFAULT=.env

ENV_FILE=${1:-$DEFAULT}

if [ -f "$ENV_FILE" ]; then
  echo "Setting Up Environment from ${ENV_FILE}"
  export $(egrep -v '^#' ${ENV_FILE} | xargs)
fi

ts-node-dev -r tsconfig-paths/register main.ts
