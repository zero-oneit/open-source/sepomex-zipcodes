import Boot from '@zerooneit/expressive-tea/classes/Boot';
import {Pour, RegisterModule, ServerSettings} from '@zerooneit/expressive-tea/decorators/server';
import ExpressPlugin from '@server/plugins/express';
import RootModule from '@app/root/RootModule';
import {ExpressiveTeaApplication} from '@zerooneit/expressive-tea/libs/interfaces';
import MongoosePlugin from '@expressive-tea/mongoose-plugin';
import * as mongoose from 'mongoose';

MongoosePlugin.modelDirectory = `${__dirname}/server/mongodb/models/`;
MongoosePlugin.modelExtension = '*.ts';
MongoosePlugin.mongoose = mongoose;

@Pour(ExpressPlugin)
@Pour(MongoosePlugin)
@ServerSettings({
  port: process.env.PORT as unknown as number || 3000,
})
class Bootstrap extends Boot {
  @RegisterModule(RootModule)
  async start(): Promise<ExpressiveTeaApplication> {
    return super.start();
  }
}

export default new Bootstrap().start()
  .catch(e => console.error(e));
