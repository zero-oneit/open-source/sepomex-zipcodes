
import * as turf from '@turf/turf';
import { sync } from 'glob';
import { readFileSync } from 'fs';
import * as mongoose from 'mongoose';

mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true});

import { GeoJson } from '@models/GeoJson';

async function main() {

    const files = sync('**/*.geojson');

    for (const file of files) {
        const jsondata = readFileSync(file);
        const geojson = JSON.parse(jsondata.toString());
        const saveData = [];

        for (const feature of geojson.features) {
            const feat = turf.feature(feature.geometry, feature.properties);

            if (feature.geometry.type === 'Polygon') {
                const center = turf.center(feature);
                saveData.push({ postal_code: feat.properties.d_cp, boundaries: feat.geometry, location: center.geometry });
            } else if (feature.geometry.type === 'GeometryCollection') {
                for (const geom of feature.geometry.geometries) {
                    const center = turf.center(geom);
                    saveData.push({ postal_code: feat.properties.d_cp, boundaries: geom, location: center.geometry });
                }
            }

        }

        await GeoJson.insertMany(saveData);
    }

}

main().then(() => mongoose.disconnect());
