import { deburr, values } from 'lodash';
import { createReadStream, createWriteStream } from 'fs';
import * as csv from 'csv-parser';
import * as mongoose from 'mongoose';

const headers = [
  'postalCode',
  'settlementName',
  'settlementType',
  'municipality',
  'state',
  'city',
  'adminPostalCode',
  'stateCode',
  'adminPostalCodeAlt',
  'empty',
  'settlementTypeCode',
  'municipalityCode',
  'municipalitySettlementCode',
  'zone',
  'cityCode'
];

const results: any[] = [];
const states: any = {};
const municipalities: any = {};
const cities: any = {};
const settlementTypes: any = {};
let unkCityCode = 1;

createReadStream('data/postal_code.txt')
  .pipe(csv({
    separator: '|',
    headers, skipLines: 1,
    mapValues: ({ header, index, value }) => deburr(value)
  }))
  .on('data', data => {
    results.push(data);
    if (!settlementTypes[data.settlementType]) {
      settlementTypes[data.settlementType] = {
        settlementType: data.settlementType,
        city: data.city,
        cityCode: data.cityCode,
        state: data.state,
        municipality: data.municipality
      };
    }

    const stateKey = data.state;

    if (!states[stateKey]) {
      states[data.state] = {
        state: data.state,
        stateCode: data.stateCode
      };
    }

    const municipalityKey = [data.state, data.municipality].join(':');

    if (!municipalities[municipalityKey]) {
      municipalities[municipalityKey] = {
        stateCode: data.stateCode,
        municipality: data.municipality,
        municipalityCode: data.municipalityCode
      };
    }

    const city = !data.city ? [data.settlementType, data.settlementName].join(' ') : data.city;
    const cityKey = [data.state, data.municipality, city].join(':');

    if (!cities[cityKey]) {
      cities[cityKey] = {
        stateCode: data.stateCode,
        municipalityCode: data.municipalityCode,
        city,
        cityCode: data.cityCode || `U${unkCityCode}`
      };
      if (!data.cityCode) {
        unkCityCode++;
      }
    }

  })
  .on('end', async () => {
    try {
      const stateCypher = createWriteStream('data/state.cypher', {
        flags: 'w' // 'a' means appending (old data will be preserved)
      });

      stateCypher.write('// States Creation \n');

      for (const st of values(states)) {
        if (!st) {
          continue;
        }
        stateCypher.write(`CREATE (ST${st.stateCode}:STATE {name: "${st.state}", code: "${st.stateCode}"})\n`);
      }

      stateCypher.write('\n// Municipalities Creation \n\n');
      const stEntities = [];
      const stRelationships = [];
      for (const mn of values(municipalities)) {
        if (!mn) {
          continue;
        }
        const stateKey = `ST${mn.stateCode}`;
        const munKey = `ST${mn.stateCode}MN${mn.municipalityCode}`;
        // @ts-ignore
        stEntities.push(`CREATE (${munKey}:MUNICIPALITY {name: "${mn.municipality}", code: "${mn.municipalityCode}"})`);
        stRelationships.push(`(${munKey}) -[:BELONGS_TO]-> (${stateKey})`);
      }
      stateCypher.write(stEntities.join('\n'));
      stateCypher.write(`\n\nCREATE ${stRelationships.join(',\n')}`);

      stateCypher.write('\n// Cities Creation \n\n');

      const ctEntities = [];
      const ctRelationships = [];
      for (const mn of values(cities)) {
        if (!mn) {
          continue;
        }
        const stateKey = `ST${mn.stateCode}MN${mn.municipalityCode}CT${mn.cityCode}`;
        const munKey = `ST${mn.stateCode}MN${mn.municipalityCode}`;

        // @ts-ignore
        ctEntities.push(`CREATE (${stateKey}:CITY {name: "${mn.city}", code: "${mn.cityCode}"})`);
        ctRelationships.push(`(${stateKey}) -[:BELONGS_TO]-> (${munKey})`);
      }

      stateCypher.write(ctEntities.join('\n'));
      stateCypher.write(`\n\nCREATE ${ctRelationships.join(',\n')}`);
    } catch (e) {
    }
    console.log('---Finished---');
    await mongoose.disconnect();
  });
