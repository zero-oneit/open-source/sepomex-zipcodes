import {Module} from '@zerooneit/expressive-tea/decorators/module';
import RootController from '@app/root/controllers/RootController';
import StatesController from './controllers/StatesController';

@Module({
  controllers: [RootController, StatesController],
  providers: [],
  mountpoint: '/'
})
export default class RootModule {
}
