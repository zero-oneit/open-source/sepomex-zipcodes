import * as mongoose from 'mongoose';
import { Get, Route } from '@zerooneit/expressive-tea/decorators/router';

@Route('/')
export default class RootController {
    @Get('/')
    async index() {
        const PostalCode = mongoose.model('postalcode');
        return PostalCode
          .find()
          .populate('geometry')
          .populate('state', '-_id -__v')
          .populate('city', '-_id -__v -stateCode -municipalityCode')
          .populate('municipality', '-_id -__v -stateCode')
          .limit(20);
    }
}
