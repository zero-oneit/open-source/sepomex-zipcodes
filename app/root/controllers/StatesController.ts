import { Get, Param, Route } from '@zerooneit/expressive-tea/decorators/router';
import { next, param, query, request } from '@zerooneit/expressive-tea/decorators/annotations';
import { BadRequestException } from '@zerooneit/expressive-tea/exceptions/RequestExceptions';
import { State, Municipality, City } from '@models/PostalCode';
import { size } from 'lodash';
import { NextFunction, Request } from 'express';

interface StateRequest extends Request {
  state: any;
}

@Route('/states')
export default class StatesController {
  @Param('stateCode')
  async populateStateCode(
    @request req: StateRequest,
    @param('stateCode') stateCode: string,
    @next next: NextFunction
  ) {

    if (!stateCode) {
      return next(new BadRequestException('State Code is empty'));
    }
    req.state = await State.findOne({ stateCode }).select('state stateCode _id').lean(true);
    next();
  }

  @Get('/')
  async index(@query('q') q: string) {
    const query: any = {};

    if (size(q) < 3) {
      throw new BadRequestException('Query must contain at least 3 characters');
    }

    if (q) {
      query.state = { $regex: `^.*${q}.*$`, $options: 'ims' };
    }

    return State.paginate(query, {
      select: '-id -__v -_id',
      sort: 'stateCode',
      lean: true,
      pagination: false,
      customLabels: { totalDocs: 'totalStates', docs: 'states' }
    });
  }

  @Get('/byId/:stateId')
  async byId(@param('stateId') stateId: string) {
    if (!stateId) {
      throw new BadRequestException('State Id is empty');
    }

    return State.findById(stateId);
  }

  @Get('/:stateCode')
  async byCode(@request req: StateRequest) {
    return req.state;
  }

  @Get('/:stateCode/municipalities')
  async getMunicipalities(
    @param('stateCode') stateCode: string,
    @query('page') page: number = 1,
    @query('limit') limit: number = 10
  ) {
    return Municipality.paginate({ stateCode }, {
      select: '-id -__v -_id',
      sort: 'municipalityCode',
      page,
      limit,
      lean: true,
      customLabels: { totalDocs: 'totalMunicipities', docs: 'municipalities' }
    });
  }

  @Get('/:stateCode/municipalities/:municipalityCode')
  async getMunicipality(
    @param('stateCode') stateCode: string,
    @param('municipalityCode') municipalityCode: string
  ) {
    return Municipality.findOne({ stateCode, municipalityCode});
  }

  @Get('/:stateCode/municipalities/:municipalityCode/cities')
  async getCities(
    @param('stateCode') stateCode: string,
    @param('municipalityCode') municipalityCode: string,
    @query('page') page: number = 1,
    @query('limit') limit: number = 10
  ) {
    return City.paginate({ stateCode, municipalityCode }, {
      select: '-id -__v -_id',
      sort: 'municipalityCode',
      page,
      limit,
      lean: true,
      customLabels: { totalDocs: 'totalCities', docs: 'cities' }
    });
  }
}
