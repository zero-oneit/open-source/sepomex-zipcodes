import * as mongoose from 'mongoose';
import GeoJsonSchema from '../schemas/GeoJsonSchema';

export const GeoJson = mongoose.model('geojson', GeoJsonSchema);
