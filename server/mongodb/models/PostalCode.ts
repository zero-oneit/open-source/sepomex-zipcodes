import * as mongoose from 'mongoose';
import * as PaginatePlugin from 'mongoose-paginate-v2';
import CitySchema from '../schemas/CitySchema';
import MunicipalitySchema from '../schemas/MunicipalitySchema';

import PostalCodeSchema from '../schemas/PostalCodeSchema';
import StateSchema from '../schemas/StateSchema';

PostalCodeSchema.plugin(PaginatePlugin);
StateSchema.plugin(PaginatePlugin);
MunicipalitySchema.plugin(PaginatePlugin);
CitySchema.plugin(PaginatePlugin);

export const PostalCode = mongoose.model('postalcode', PostalCodeSchema);
export const State = mongoose.model('state', StateSchema);
export const Municipality = mongoose.model('municipality', MunicipalitySchema);
export const City = mongoose.model('city', CitySchema);
