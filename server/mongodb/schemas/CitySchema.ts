import { Schema } from 'mongoose';

const CitySchema = new Schema({
    stateCode: {type: String, index: true},
    municipalityCode: String,
    city: {type: String, index: true},
    cityCode: {type: String, index: true},
});

CitySchema.index({ city: 'text' },{ default_language: 'spanish' });

export default CitySchema;