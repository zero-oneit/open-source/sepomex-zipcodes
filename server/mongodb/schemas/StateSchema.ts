import { Schema } from 'mongoose';

const StateSchema = new Schema({
    state: {type: String, index: true},
    stateCode: {type: String, index: true}
});

StateSchema.index({state: 'text' },{ default_language: 'spanish' });

export default StateSchema;