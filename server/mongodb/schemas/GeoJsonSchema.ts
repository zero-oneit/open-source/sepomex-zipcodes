import 'mongoose-geojson-schema';
import { Schema } from 'mongoose';

const GeoJsonSchema = new Schema({
  postal_code: {
    type: String,
    required: true,
    index: true
  },
  location: {
    // @ts-ignore
    type: Schema.Types.Point,
    required: true,
    index: '2dsphere'
  },
  boundaries: {
    // @ts-ignore
    type: Schema.Types.Polygon,
    required: true
  }
});

export default GeoJsonSchema;
