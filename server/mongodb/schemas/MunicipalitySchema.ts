import { Schema } from 'mongoose';

const MunicipalitySchema = new Schema({
    municipality: {type: String, index: true},
    stateCode: {type: String, index: true},
    municipalityCode: String
});

MunicipalitySchema.index({ municipality: 'text' },{ default_language: 'spanish' });

export default MunicipalitySchema;