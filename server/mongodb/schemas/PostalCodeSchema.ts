import { Schema } from 'mongoose';

const PostalCodeSchema = new Schema({
  postalCode: {
    type: String,
    required: true,
    index: true
  },
  settlementName: { type: String, index: true },
  settlementType: String,
  adminPostalCode: String,
  stateCode: String,
  adminPostalCodeAlt: String,
  settlementTypeCode: String,
  municipalityCode: String,
  municipalitySettlementCode: String,
  zone: String,
  cityCode: String
}, {
  toJSON: { virtuals: true },
  toObject: { virtuals: true }
});

PostalCodeSchema.virtual('state', {
  ref: 'state',
  localField: 'stateCode',
  foreignField: 'stateCode',
  justOne: true
});

PostalCodeSchema.virtual('city', {
  ref: 'city',
  localField: 'cityCode',
  foreignField: 'cityCode',
  justOne: true
});

PostalCodeSchema.virtual('municipality', {
  ref: 'municipality',
  localField: 'municipalityCode',
  foreignField: 'municipalityCode',
  justOne: true
});

PostalCodeSchema.virtual('geometry', {
  ref: 'geojson',
  localField: 'postalCode',
  foreignField: 'postal_code',
  justOne: false
});

PostalCodeSchema.index({ settlementName: 'text' }, { default_language: 'spanish' });

export default PostalCodeSchema;
