import { BOOT_STAGES, Plugin } from '@expressive-tea/plugin';
import { Stage } from '@expressive-tea/plugin/decorators';
import * as glob from 'glob';
import * as mongoose from 'mongoose';
import * as path from 'path';

export default class MongoDBPlugin extends Plugin {
  protected name: string = 'MongoDB Plugin';
  protected priority: number = -1;
  protected readonly modelExtension: string;
  protected readonly directory: string;

  constructor(modelDirectory: string = path.resolve(__dirname, '/models'), extension: string = '*Model.ts') {
    super();
    this.modelExtension = extension;
    this.directory = modelDirectory;
  }

  @Stage(BOOT_STAGES.BOOT_DEPENDENCIES, true)
  async initialize() {
    await mongoose.connect(process.env.MONGO_URL, {
      poolSize: 10,
      useCreateIndex: true,
      useFindAndModify: true,
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    const models: string[] = glob.sync(path.resolve(__dirname, './models/*.ts')) || [];
    for (const model of models) {
      await import(model);
    }
  }
}
